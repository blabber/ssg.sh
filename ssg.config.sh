TITLE='ssg.sh Demo'

TEMPLATE='cat <<-EOF
		<!DOCTYPE html>
		<html>
		<head>
		    <link rel="stylesheet" href="${CSS_HREF}">
		
		    <meta charset="utf-8">
		    <meta name="viewport"
		          content="width=device-width, initial-scale=1.0">
		
		    <title>${TITLE} - ${PAGETITLE}</title>
		</head>
		
		<body>
		    <header>
		        <nav>
		            <ul>
		                <li><a href="https://blabber.codeberg.page/ssg.sh/">
		                    Home
		                </a></li>
		                <li><a href="https://codeberg.org/blabber/ssg.sh/">
		                    Source
		                </a></li>
		            </ul>
		        </nav>

		        <h1>${TITLE}</h1>
		        <h2>${PAGETITLE}</h2>

		        <hr/>
		    </header>

		    <main>
		        ${BODY}
		    </main>

		    <footer>
		        <hr/>
		        <p>
		            <small>
		                generated on $(date) using
		                <a href="https://codeberg.org/blabber/ssg.sh">ssg.sh</a>
		            </small>
		        </p>
		    </footer>
		</body>
		</html>
	EOF'

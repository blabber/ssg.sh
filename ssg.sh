#!/bin/sh

# "THE BEER-WARE LICENSE" (Revision 42):
# <tobias.rehbein@web.de> wrote this file. As long as you retain this notice
# you can do whatever you want with this stuff. If we meet some day, and you
# think this stuff is worth it, you can buy me a beer in return. Tobias Rehbein

# Activate Bash Strict Mode
# - although this is explicitly NOT a bash script ;)
set -euo pipefail

MARKDOWN='markdown_py'
SRCDIR='./src'
OUTDIR='./out'

CSS='./css/mvp.css'

TITLE='The Title'

TEMPLATE='cat <<-EOF
		<!DOCTYPE html>
		<html>
		<head>
		    <link rel="stylesheet" href="${CSS_HREF}">
		
		    <meta charset="utf-8">
		    <meta name="viewport"
		          content="width=device-width, initial-scale=1.0">
		
		    <title>${TITLE} - ${PAGETITLE}</title>
		</head>
		
		<body>
		    <main>
		        ${BODY}
		    </main>
		</body>
		</html>
	EOF'

# All these variables can be overriden in an optional ssg.config.sh file.
if [ -f './ssg.config.sh' ]; then
	. './ssg.config.sh'
fi

IFS='
'

_generate_index_entries() {
	local _dir _prefix _trail
	_dir="${1}"
	_prefix="${2}"
	if [ -z "${3}" ]; then
		_trail="./"
	else
		_trail="${3}${_dir}/"
	fi

	local _line
	(cd "${_dir}" &&
		find . -mindepth 1 -maxdepth 1 -type d \! -path './.git' |
			sort |
			cut -d '/' -f 2 - | while read _line; do
				printf '%s %s\n' "${_prefix}" $(basename ${_line})
				_generate_index_entries "${_line}" "    ${_prefix}" "${_trail}"
			done &&

		find . -mindepth 1 -maxdepth 1 -type f -name '*.md'  \! -path './.git' \! -name 'index.md' |
			sort |
			cut -d '/' -f 2 - | while read _line; do
				local _name _link
				_name=$(echo "${_line}" | sed 's/\.md$//')
				_link="${_trail}${_line}"
				printf '%s [%s](%s)\n' "${_prefix}" $(basename "${_name}") "${_link}"
			done)
}

generate_index() {
	local _dir
	_dir="${1}"

	printf '# %s\n\n' "${TITLE}"
	_generate_index_entries "${_dir}" "*" ""
}

generate_site() {
	local _indir _outdir _infile _outfile
	_indir="${1}"
	_outdir="${2}"


	for _infile in $(find "${_indir}" -type f -name '*.md'); do
		_outfile=$(echo "${_infile}" | sed -e "s:^${_indir}/:${_outdir}/:" -e 's:\.md$:.html:')
		mkdir -p $(dirname "${_outfile}")

		PAGETITLE="$(basename "${_infile}" | sed 's:\.md$::')"

		CSS_HREF=$(dirname "${_outfile}" | sed -e 's:^\./::' -e "s:^${_outdir}::" -e 's:[^/]*::g')
		CSS_HREF=$(echo "${CSS_HREF}" | sed 's:/:../:g')
		if [ -z "${CSS_HREF}" ]; then
			CSS_HREF='./'
		fi
		CSS_HREF="${CSS_HREF}${CSS}"

		BODY=$(sed 's:\.md\>:.html:' < "${_infile}" | "${MARKDOWN}")

		eval "$TEMPLATE" > "${_outfile}"
	done
}

generate_index "${SRCDIR}" > "${SRCDIR}/index.md"

[ -d "${OUTDIR}" ] && rm -r "${OUTDIR}"
mkdir -p "${OUTDIR}"
cp -r $(dirname "${CSS}") "${OUTDIR}"

generate_site "${SRCDIR}" "${OUTDIR}"
